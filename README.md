# Challenge Datenn

## Intro

We strived to create a challenge that spreads accross most of the areas a DevOps Engineer will be working at Datenn. In reality this involves too many things for us to simulate here, namely: setting up infrastructure-as-code for cloud environments, bash/python/go scripts, developping our backend java/go, and every once in a while touch the frontend React app to fix bugs or customize things. This challenge focuses on the infrastructure as code, and go coding, thus we split it into several stages:

- Build a simple application in go;
- Dockerize it - wrap in it a docker container, so that it can be run (almost) anywhere;
- Deploy it - write infrastructure-as-code that deploys this application in cloud environment;


## Challenge details

### Stage 1 - build a simple app

Here are the requirements:

- The app should be written in Go. 
- The app should list all AWS S3 buckets in a given Region; this region should be provided as a parameter to the application, for example as an environmental variable;
- The app should print to standard out a new line for each bucket;
- No credentials should be hardcoded anywhere! The developer running the App should inject his own credentials!


Here are some of the things we value, though not mandatory:
- Tests, unit and/or integration;
- Documentation (readme, for example) how to run the code/ purpose of the code, etc. Ideally simple and consise, not a novel;
- A clean git history; for example, we much prefer rebasing, and prefer separating each logical feature in different commits kind of in a atomic & transaction-like manner. This way, if in the future we realize we do not need a given feature, we simply need to drop that commit



### Stage 2 - dockerize it


Here are the requirements:
- The docker container should be a flavour of linux, such as ubuntu, or centos; 
- By running the container, it should run the code in your go application; if one inspects the logs of your container, one should be able to see the results printed there;

Here are some of the things we value, though not mandatory:
- An easy way to run your docker container (for example Makefile or bash script);
- Documentation (readme, for example) how to run the code/ purpose of the code, etc. Ideally simple and consise, not a novel;
- A clean git history; for example, we much prefer rebasing, and prefer separating each logical feature in different commits kind of in a atomic & transaction-like manner. This way, if in the future we realize we do not need a given feature, we simply need to drop that commit



### Stage 3 - deploy it in the cloud


Here are the requirements:
- Write Terraform code (feel free to choose any version you prefer of terraform) that deploys your code in a AWS Lambda function.  

Here are some of the things we value, though not mandatory:
- An easy way to run terraform code;
- Documentation (readme, for example) how to run the code/ purpose of the code, etc. Ideally simple and consise, not a novel;
- A clean git history; for example, we much prefer rebasing, and prefer separating each logical feature in different commits kind of in a atomic & transaction-like manner. This way, if in the future we realize we do not need a given feature, we simply need to drop that commit


## And now what ?

Please send us a zip with your solution. Please do not create a pull request or fork this repository as your solution should not be end up being public afterwards.

Good luck and happy coding!



